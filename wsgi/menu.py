#coding: utf-8

# 第一層標題
m1 = "<a href='/about'>有關本站</a>"
m11 = "<a href='/'>Brython程式區</a>"
m12 = "<a href=''>四連桿機構</a>"

# 第一層標題
m2 = "<a href=''>Creo</a>"
m21 = "<a href=''>Creo零件</a>"
m22 = "<a href=''>Creo組合</a>"
m23 = "<a href=''>Pro/Web.Link操作</a>"
# 第一層標題
m3 = "<a href=''>Solvespace</a>"
m31 = "<a href=''>Solvespace零件</a>"
m311 = "<a href=''>Solvespace組合</a>"
# 第一層標題
m4 = "<a href=''>V-REP</a>"
m41 = "<a href=''>V-REP操作</a>"
# 第一層標題
m5 = "<a href=''>組員介紹</a>"
m51 = "<a href='/introMember1'>09號-劉凱盈</a>"
m52 = "<a href='/introMember2'>20號-李晉杰</a>"
m53 = "<a href='/introMember3'>22號-林立程</a>"
m54 = "<a href='/introMember4'>31號-張智閔</a>"
m55 = "<a href='/introMember5'>32號-梁冠緯</a>"
m56 = "<a href='/introMember6'>53號-謝子傑</a>"
# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 = [ \
        [m1, [m11, m12], ], \
        [m2, [m21],m22,m23], \
        [m3, [m31, ], m311 ], \
        [m4, [m41, ], ], \
        [m5, m51, m52, m53, m54,m55,m56]
        ]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
